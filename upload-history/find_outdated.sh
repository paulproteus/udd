#!/bin/bash

ls *.out &>/dev/null || exit 0
for i in *.out; do
   [ -f /home/debian/lists/debian-devel-changes/$(basename $i .out) ] || rm -f $i
done
