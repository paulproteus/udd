#!/usr/bin/python
import argparse
import email
import email.utils
import gzip
import lzma
import mailbox
from pathlib import Path
import os
import re
import shutil
import subprocess
import sys

from debian import deb822

keyid_regex = re.compile(rb"\[GNUPG:\] ([A-Z]+)SIG (\S+) (.+)")


def get_key_info_gpgv(keyring_path, msg_bytes):
    """ Verify given msg with gpgv using the Debian keyring files in the given path

    Return a dictionary with detected fields """

    # note: gpgv emits VALIDSIG also for expired (sub)keys and we rely on this
    # fact to get the fingerprint anyway, gpg --verify seems to act differently
    cmd = [shutil.which("gpgv"), "--status-fd", "1"]

    keyring_list = [
        Path(keyring_path / "debian-keyring.gpg").absolute(),
        Path(keyring_path / "debian-maintainers.gpg").absolute(),
        Path(keyring_path / "emeritus-keyring.gpg").absolute(),
    ]
    for keyring in keyring_list:
        if not keyring.exists():
            raise ValueError(
                "Unable to find {keyring} in {keyring_path}".format(keyring=keyring, keyring_path=keyring_path) +
                "Please download the Debian GPG keys before running this program. Consider:\n\n"
                "rsync --recursive --checksum keyring.debian.org::keyrings/keyrings/ ."
            )

    for keyring in keyring_list:
        cmd.extend(["--keyring", str(keyring)])

    p = subprocess.Popen(cmd, stdin=subprocess.PIPE, stderr=subprocess.PIPE,
                         stdout=subprocess.PIPE, env={"LC_ALL": "C", "GNUPGHOME": os.curdir})

    (out, err) = p.communicate(msg_bytes)

    # Create default return value; overwrite as we parse the GPG output.
    result = {'Fingerprint': 'N/A', 'Key': 'N/A', 'Signed-By': 'N/A'}

    out_lines = out.split(b'\n')

    for line in out_lines:
        if line.startswith(b"[GNUPG:] NODATA 1"):
            return {}

        # VALIDSIG <space-separated fields> fingerprint
        if line.startswith(b"[GNUPG:] VALIDSIG"):
            result['Fingerprint'] = (line.split(b' ')[-1]).decode('ascii')

        m = keyid_regex.search(line)
        if m:
            # see /usr/share/doc/gnupg/DETAILS.gz
            # fields with <longuid> <username> format
            if m.group(1) in (b"GOOD", b"EXP", b"EXPKEY", b"REVKEY", b"BAD"):
                result['Key'] = m.group(2).decode('ascii')
                result['Signed-By'] = m.group(3).decode('utf-8', 'replace')
            # ERRSIG  <long keyid>  <pubkey_algo> <hash_algo> <sig_class> <timestamp> <rc>
            elif m.group(1) == "ERR":
                result['Key'] = m.group(2).decode('ascii')

    return result


def normalize_date(date):
    d = email.utils.parsedate_tz(date)
    if not d:
        return None

    try:
        ret = email.utils.formatdate(email.utils.mktime_tz(d), True)
    except OverflowError:
        ret = None

    return ret


def build_keyring_list(dir):
    """ Scan dir and return a list of every file ending with .gpg or .pgp """
    keyrings = []
    for root, dirs, files in os.walk(dir):
        keyrings.extend([os.path.join(root, x) for x in files if x.endswith(".gpg") or x.endswith(".pgp")])

    return keyrings


# Two regular expressions to help detect non-maintainer uploads.
# The first gives some false positives for example with -x+y.z.w.
# The second allows us to decrease false positives by searching the changelog line.
# DEP1 https://dep-team.pages.debian.net/deps/dep1.html standardizes this for 2008 and later.
nmu_version_regex = re.compile(r"(-\S+\.\d+|\+nmu\d+)$")
nmu_changelog_regex = re.compile(r"\s+\*\s+.*(NMU|non[- ]maintainer)", re.IGNORECASE + re.MULTILINE)

bug_number_list_regex = re.compile("^([0-9]{3,} ?)+")


def handle_file(mbox_path, output_path, keyring_dir):
    """For each message in mbox_path (as a unix mailbox), look for info about a Debian upload.

    If we've already processed a message with this message ID, skip it. This permits the input mbox files to have
    some overlap in messages.

    Decompress mbox_path with gzip or lzma, as needed."""

    # Create a Python mailbox object, but overwrite its internal file object to our mailbox. This allows us to
    # decompress mbox data on the fly.
    mb = mailbox.mbox('/dev/null', create=False)
    if mbox_path.name.endswith(".gz"):
        mb._file = gzip.open(mbox_path)
    elif mbox_path.name.endswith(".xz"):
        mb._file = lzma.open(mbox_path)
    else:
        mb._file = open(mbox_path)

    for message in mb:
        # Skip multipart messages; we're only interested in plain text messages which inline a Debian .changes file.
        if message.is_multipart():
            continue

        body_lines = message.get_payload().split('\n')

        # Allow deb822.Changes to seek forward in the message and find the metadata. Skip "Hash: sha256" line.
        for (i, l) in enumerate(body_lines):
            if l.startswith("Format:"):
                break
        changes = deb822.Changes(body_lines[i:])

        message_id = message.get('message-id')
        if (not set(['Source', 'Architecture', 'Version', 'Date', 'Changes']).issubset(changes.keys()) or
                'source' not in changes.get('Architecture', '')):
            sys.stderr.write(
                "Required fields not found in message {message_id}, skipping.\n".format(message_id=message_id))
            continue

        # Start with data from email message
        upload_info = {
            'Message-Id': message_id.strip('\n'),
            'Message-Date': message['Date'].strip('\n'),
        }

        # Add data from changes metadata within the message
        for field in [
                'Source', 'Architecture', 'Version', 'Date', 'Changed-By', 'Maintainer', 'Closes',  'Distribution']:
            upload_info[field] = changes.get(field, 'N/A')
        upload_info['NMU'] = str(
            (nmu_version_regex.search(upload_info['Version']) is not None) and
            (nmu_changelog_regex.search(changes['Changes']) is not None)
        )
        bug_list_match = bug_number_list_regex.match(upload_info['Closes'])
        upload_info['Closes'] = bug_list_match.group() if bug_list_match else 'N/A'

        # Add Fingerprint, Key, Signed-By
        upload_info.update(get_key_info_gpgv(keyring_dir, ('\n'.join(body_lines)).encode('utf-8')))

        # Remove date if it cannot be parsed
        d = normalize_date(upload_info['Date'])
        if not d:
            upload_info['Date'] = "N/A"

        # Append output to output_path
        output_data = ""
        for field in upload_info:
            output_data += "{field}: {value}\n".format(field=field, value=upload_info[field])
        # separate different packages
        output_data += "\n"

        with output_path.open('a') as output_fd:
            output_fd.write(output_data)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Scan a series of mbox files for Debian uploads; create *.deb822-list files for "
                    "upload_history_importer.py to consume.\n\n"
                    "Download keyring data with: \n"
                    "  $ mkdir keyring-dir"
                    "  $ rsync --recursive --checksum keyring.debian.org::keyrings/keyrings/. keyring-dir/. "
    )
    parser.add_argument('--mbox', nargs="+", required=True)
    parser.add_argument('--output-dir', required=True)
    parser.add_argument('--keyring-dir', required=True)
    args = parser.parse_args()
    for mbox in args.mbox:
        mbox_path = Path(mbox)
        output_path = Path(args.output_dir) / (mbox_path.name + ".deb822-list")
        handle_file(mbox_path, output_path, Path(args.keyring_dir))

# vim: et:ts=4:sw=4
